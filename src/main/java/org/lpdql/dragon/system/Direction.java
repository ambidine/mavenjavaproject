package org.lpdql.dragon.system;

public enum Direction {
    HORIZONTAL,
    VERTICAL,
    RANDOM,
    IMMOBILE;
}
